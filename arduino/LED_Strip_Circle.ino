
#include <FastLED.h>

#define NUM_LEDS 192
#define DATA_PIN A5
#define SPACING 15
CRGB leds[NUM_LEDS];

void setup() {
  Serial.begin(9600);
  FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);

  for (int i = 0; i < NUM_LEDS; i += SPACING) {
    leds[i] = CRGB::White;

    FastLED.show();
  }

  delay(200);
}

void loop() {
  
  trail(getRandomColor());
  trail(getRandomColor());
  fadeAway(getRandomColor());
  fadeAway(getRandomColor());

  for (int i = 0; i < 500; i++) {
    rotateLeftColorWheel(getRandomColor());
  }
  rotateLeftTrails();
  for (int i = 0; i < 500; i++) {
    rotateLeftColorWheel(getRandomColor());
  }
  rotateLeftTrails();
  
}

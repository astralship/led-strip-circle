#define ROTATE_SPEED 20

void rotateLeftColorWheel(CRGB color) {

  CRGB temp[1];
  temp[0] = leds[0];
  for (int i = 0; i < NUM_LEDS - 1; i++) {
    leds[i] = leds[i + 1];
  }
  //leds[NUM_LEDS-1] = temp[0];
  leds[NUM_LEDS - 1] = color;
  FastLED.show();
  delay(ROTATE_SPEED);
}

void rotateLeftTrails(void) {
  turnAllLeds(CRGB::Black);
  leds[0] = getRandomColor();
  leds[1] = getRandomColor();
  leds[2] = getRandomColor();
  leds[100] = getRandomColor();
  leds[101] = getRandomColor();
  leds[102] = getRandomColor();
  for (int j=0; j<500; j++) {
  CRGB temp[1];
  temp[0] = leds[0];
  for (int i = 0; i < NUM_LEDS - 1; i++) {
    leds[i] = leds[i + 1];
  }
  leds[NUM_LEDS - 1] = temp[0];
  FastLED.show();
  delay(ROTATE_SPEED);  
  }
  
}

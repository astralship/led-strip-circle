#define BLINK_SPEED 10000

void blinky() {
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB::White; 
  }
  FastLED.show();
  delay(BLINK_SPEED);

  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB::Black;
  }
  FastLED.show();
  delay(BLINK_SPEED);
}

# LED Strip Circle

Arduino code to configure the LED strip shapped in a circle used in the AstralShip

## Getting Started

### Prerequisites

- Download the [Arduino IDE](https://www.arduino.cc/en/Main/Software)

### Installing

- Clone this repository
- Open LED_Strip_Circle.ino with Arduino IDE, other .ino files should automatically open.
- Install the [FastLED](https://github.com/FastLED/FastLED) Library :  https://www.arduino.cc/en/Guide/Libraries

#define TRAIL_SIZE 10
#define TRAIL_SPEED 15

void trail(CRGB color) {
  //For each LED
  for (int lastLed = 0; lastLed < NUM_LEDS; lastLed++) {
    int firstLed = lastLed - TRAIL_SIZE;
    if (firstLed < 0) {
      firstLed = NUM_LEDS + firstLed;
    }
    for (int i = firstLed; i <= lastLed; i++) {
      int l = i % NUM_LEDS;
      leds[l] = color;
    }
    leds[firstLed] = CRGB::Black;

    FastLED.show();
    delay(TRAIL_SPEED);
  }
}

#define BREATHING_SPEED 100
#define BREATHING_DECREMENT 2

void fadeAway(CRGB color) {
  turnAllLeds(color);
  delay(BREATHING_SPEED);

  CRGB newColor = color;
  while (newColor.getAverageLight() > 4) {
    delay(BREATHING_SPEED);
    newColor = newColor.nscale8(250);
    turnAllLeds(newColor);
  }
  turnAllLeds(CRGB::Black);
}

void turnAllLeds(CRGB color) {
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = color;
  }
  FastLED.show();
}

CRGB getRandomColor() {
  int r = random(10, 50);
  int g = random(10, 50);
  int b = random(10, 50);
  return CRGB(r, g, b);
}

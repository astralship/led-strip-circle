#include "patterns.h"

CRGB leds[NUM_LEDS];

int patternCode = 0;

void fadeAway(CRGB color) {
  turnAllLeds(color);
  delay(BREATHING_SPEED);

  CRGB newColor = color;
  while (newColor.getAverageLight() > 4) {
    delay(BREATHING_SPEED);
    newColor = newColor.nscale8(250);
    turnAllLeds(newColor);
  }
  turnAllLeds(CRGB::Black);
}

void turnAllLeds(CRGB color) {
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = color;
  }
  FastLED.show();
}

void blinky(void) {
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB::White; 
  }
  FastLED.show();
  delay(BLINK_SPEED);

  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = CRGB::Black;
  }
  FastLED.show();
  delay(BLINK_SPEED);
}

CRGB getRandomColor(void) {
  int r = random(MIN_BRIGHTNESS, MAX_BRIGHTNESS);
  int g = random(MIN_BRIGHTNESS, MAX_BRIGHTNESS);
  int b = random(MIN_BRIGHTNESS, MAX_BRIGHTNESS);
  return CRGB(r, g, b);
}

void rotateLeftColorWheel(CRGB color) {

  CRGB temp[1];
  temp[0] = leds[0];
  for (int i = 0; i < NUM_LEDS - 1; i++) {
    leds[i] = leds[i + 1];
  }
  //leds[NUM_LEDS-1] = temp[0];
  leds[NUM_LEDS - 1] = color;
  FastLED.show();
  delay(ROTATE_SPEED);
}

void rotateLeftTrails(void) {
  turnAllLeds(CRGB::Black);
  leds[0] = getRandomColor();
  leds[1] = getRandomColor();
  leds[2] = getRandomColor();
  leds[100] = getRandomColor();
  leds[101] = getRandomColor();
  leds[102] = getRandomColor();
  for (int j=0; j<500; j++) {
  CRGB temp[1];
  temp[0] = leds[0];
  for (int i = 0; i < NUM_LEDS - 1; i++) {
    leds[i] = leds[i + 1];
  }
  leds[NUM_LEDS - 1] = temp[0];
  FastLED.show();
  delay(ROTATE_SPEED);  
  }
  
}

void trail(CRGB color) {
  //For each LED
  for (int lastLed = 0; lastLed < NUM_LEDS; lastLed++) {
    int firstLed = lastLed - TRAIL_SIZE;
    if (firstLed < 0) {
      firstLed = NUM_LEDS + firstLed;
    }
    for (int i = firstLed; i <= lastLed; i++) {
      int l = i % NUM_LEDS;
      leds[l] = color;
    }
    leds[firstLed] = CRGB::Black;

    FastLED.show();
    delay(TRAIL_SPEED);
  }
}


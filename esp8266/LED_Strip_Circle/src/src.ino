#include "patterns.h"
#include "wifi.h"
#include "web_server.h"
#include "ota.h"

#define DATA_PIN 2 // GPIO number the leds are connected to.

void setup() {
  Serial.begin(115200); // open serial port.
  // delay(5000); // give time for serial monitor to load.
  
  FastLED.addLeds <NEOPIXEL, DATA_PIN> (leds, NUM_LEDS); // create the array of leds for the FastLED library
  // tell fastLED we have NEOPIXELs on a datapin, and a certain number of them.
  
  delay(200);
  // open the program with a simple pattern.
  for (int i = 0; i < NUM_LEDS; i += SPACING) {
    leds[i] = CRGB::Amethyst;
    FastLED.show();
    delay(1);
  }
  
  wifi_setup();
  web_server_setup();
  ota_setup();
  // delay(2000);
}

void loop() {
  wifi_loop();
  web_server_loop();
  ota_loop();

  // patterns on loop:
  /*
  if (patternCode == 0) {
  fadeAway(getRandomColor());
  }
  else if(patternCode == 1) {
  trail(getRandomColor());
  }
  else if(patternCode == 2) {
    for (int j = 0; j < 2; j++) { // repeat the pattern below a few times.
        for (int i = 0; i < 500; i++) {
          rotateLeftColorWheel(getRandomColor());
        }
        rotateLeftTrails();
  }
  }
  else {
  patternCode = 0;
  }
*/

}

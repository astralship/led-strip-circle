#ifndef PATTERNS_H
#define PATTERNS_H

#include <FastLED.h>

#define NUM_LEDS 189

#define MAX_BRIGHTNESS 25
#define MIN_BRIGHTNESS 2

#define SPACING 15
#define BREATHING_SPEED 100
#define BREATHING_DECREMENT 2
#define TRAIL_SIZE 10
#define TRAIL_SPEED 15
#define BLINK_SPEED 10000
#define ROTATE_SPEED 20

CRGB getRandomColor(void);
void blinky(void);
void fadeAway(CRGB color);
void turnAllLeds(CRGB color);
void trail(CRGB color);
// rotate arrays functions:
void rotateLeftColorWheel(CRGB color);
void rotateLeftTrails(void);

extern CRGB leds[NUM_LEDS];

extern int patternCode;

#endif 
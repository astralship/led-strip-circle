This C program has been compiled with placed in 'data' folder, it is run with the associated script file, and outputs to the web_server_files directory. this is an automatic process with 'pio run'.

1 - web_server_static_files.h

    here is the list of web server files.

2 - web_server.cpp & web_server.h

    see includes listing web server files.
    the header lists content types.

3 -

    the 'data' folder contains the web server files in html, css and js format, for convenient editing.

4 -

    running hexembed.sh parses the specific html, css and js into the 'web_server_files' directory.

5 -

    hexembed.sh is in fact calling ./hexambed based on the C program.

6 -

    'pio run' calles the .sh script at line     build_flags = !./src/data/hexembed.sh

# LED Strip Circle

Arduino code to configure the LED strip shaped in a circle used in the AstralShip

## Getting Started

### Prerequisites

- For Arduino folder, download the [Arduino IDE](https://www.arduino.cc/en/Main/Software) and open LED ___ .ino
- For esp8266 folder, use platformIO.